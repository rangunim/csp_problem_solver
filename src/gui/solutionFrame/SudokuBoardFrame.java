package gui.solutionFrame;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;
import models.Variable;


public final class SudokuBoardFrame extends BoardFrame
{

    public SudokuBoardFrame(List<Variable[]> results)
    {
        super(results);
        super.setTitle("Sudoku");
        
        Variable[] result = iterator4Results.next();
        iterator4Results.previous();
        initProblemPanel(result.length);       
        super.getContentPane().add(problemPanel, BorderLayout.CENTER);
        super.pack();
    }
    
    @Override
    protected void initProblemPanel(int boardSize) 
    {
        problemPanel = new JPanel();
        int subSquareLength = (int) Math.sqrt(Math.sqrt(boardSize));
        problemPanel.setLayout(new GridLayout(subSquareLength,subSquareLength));
        problemPanel.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(3.0f)));
        
        f = new JTextField [boardSize];
        Font font1 = new Font("SansSerif", Font.PLAIN, 20);
        for(int i=0; i<f.length;i++)
        {
            f[i] = new JTextField(2);
            f[i].setHorizontalAlignment(JTextField.CENTER);
            f[i].setFont(font1);      
            f[i].setFocusable(false);
        }
       
        JPanel p[] = new JPanel[(int) Math.sqrt(boardSize)];    
        List<int[]> numerVariablesOfsubSquares = getNumberVariables4SubSquares((int) Math.sqrt(boardSize));
        for(int i =0 ; i<numerVariablesOfsubSquares.size(); i++) 
        {
            int[] subSquare = numerVariablesOfsubSquares.get(i);
            JPanel actualPanel = new JPanel();
            actualPanel.setLayout(new GridLayout(subSquareLength,subSquareLength,5,5));
            actualPanel.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(2.0f)));
            for(int j=0; j<subSquare.length;j++)
            {
                actualPanel.add(f[subSquare[j]-1]);
            }
            p[i] = actualPanel;
        }
        
        for (JPanel panel : p) 
        {
            problemPanel.add(panel);
        }
    }
    
    private List<int[]> getNumberVariables4SubSquares(int sqrtResult)
    {
        int indexNextSubSquare = 0;
        List<int[]> subSquares = new ArrayList<>(sqrtResult);
        for(int i = 1; i<=sqrtResult; i++)    
        {
            int[] subSquare = new int[sqrtResult];
            int index = 0;
            for(int j=0; j< (int) Math.sqrt(sqrtResult); j++)
            {
                for(int k = 0; k < (int) Math.sqrt(sqrtResult); k++ ) // daje kolejny podkwadrat
                {
                   //System.out.print((j * sqrtResult + k + 1 + indexNextSubSquare) + " ");
                   subSquare[index++] = j * sqrtResult + k + 1 + indexNextSubSquare; 
                }
               // System.out.println();
            }
            //System.out.println("Koniec podkwadratu " + i);
            subSquares.add(subSquare);
            if(i % (int) Math.sqrt(sqrtResult) == 0 )
            {
                indexNextSubSquare = i * sqrtResult;
            }
            else
            {
                 indexNextSubSquare += (int) Math.sqrt(sqrtResult);
            }
        }       
        return subSquares;
    }
          
    @Override
    protected void fillPanel(Variable[] result)
    {
        Variable var;
        for(int i=0; i< result.length;i++)
        {
            var = result[i];
            if(var.getDomain().size() == 1)
            {
                f[i].setForeground(Color.BLUE);
            }
            else
            {
                f[i].setForeground(Color.BLACK);
            }
            String v = String.valueOf(var.getValue());
            f[i].setText(v.equals("null") ? "" : v);
        }
    }
}
