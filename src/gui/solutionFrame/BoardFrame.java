package gui.solutionFrame;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ListIterator;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import models.Variable;


public abstract class BoardFrame  extends JFrame
{
    private static final long serialVersionUID = 0l;
   
    protected JPanel problemPanel;
    protected JTextField f[];
   
    protected JPanel actionPanel;
    
    protected final List<Variable[]> results;
    protected  ListIterator<Variable[]> iterator4Results;
    private int numberOfSolution;
    
    public BoardFrame(List<Variable[]> results)
    {
        this.results = results;
        iterator4Results = this.results.listIterator(0);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(300,300);
        this.setResizable(false);
        this.setLayout(new BorderLayout());
              
        numberOfSolution = 0;
        initActionPanel();
        
        this.getContentPane().add(actionPanel, BorderLayout.NORTH);
        this.setVisible(true);
    }
   
    private void initActionPanel()
    {
        actionPanel = new JPanel();       
        JButton previousSolutionButton  = new JButton("Poprzednie");
        JButton nextSolutionButton  = new JButton("Następne");
        numberOfSolution = 0;
        JLabel numberOfSolutionLabel = new JLabel("Rozwiązanie nr 0 /" + results.size());
        
        previousSolutionButton.setEnabled(false);
        if(iterator4Results.hasNext())
        {
            nextSolutionButton.setEnabled(true);
        }
        else
        {
            nextSolutionButton.setEnabled(false);
        }
       // if(results.size() == 1) //TODO naprawic gui i ten wylom <- zkaomentuj a other dla 1 rozwiazania wyswietli
        //{
          //  nextSolutionButton.setEnabled(false);
           // previousSolutionButton.setEnabled(false);
        //}
        
        previousSolutionButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                if(iterator4Results.hasPrevious())
                {
                    fillPanel(iterator4Results.previous());
                    numberOfSolutionLabel.setText("Rozwiązanie nr " + (--numberOfSolution) + "/" + results.size());
                    nextSolutionButton.setEnabled(true);
                }
                if(!iterator4Results.hasPrevious()  )
                {
                    previousSolutionButton.setEnabled(false);
                }
            }
        });
        actionPanel.add(previousSolutionButton);
        
        nextSolutionButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                if(iterator4Results.hasNext())
                {
                    fillPanel(iterator4Results.next());
                    numberOfSolutionLabel.setText("Rozwiązanie nr " + (++numberOfSolution) + "/" + results.size());
                    previousSolutionButton.setEnabled(true);    
                }
                if(!iterator4Results.hasNext())
                {
                    nextSolutionButton.setEnabled(false);
                }
            }
        });
        actionPanel.add(nextSolutionButton);
        actionPanel.add(numberOfSolutionLabel);
    }
    
        
    protected abstract void initProblemPanel(int boardSize); 
    protected abstract void fillPanel(Variable[] result);
}
