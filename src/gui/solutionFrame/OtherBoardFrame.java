package gui.solutionFrame;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import models.Variable;


public final class OtherBoardFrame extends BoardFrame {
    JTextArea area;

    public OtherBoardFrame(List<Variable[]> results) {
        super(results);
        super.setTitle("Inny problem");
        area = new JTextArea(10, 30);
        initProblemPanel(0);
        super.getContentPane().add(problemPanel, BorderLayout.CENTER);
        super.pack();
    }


    @Override
    protected void initProblemPanel(int boardSize) {
        problemPanel = new JPanel();
        problemPanel.setLayout(new GridLayout(0, 1));
        problemPanel.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(3.0f)));
        area.setEditable(false);
        area.setFocusable(false);
        area.setLineWrap(true);

        problemPanel.add(new JScrollPane(area));
    }

    @Override
    protected void fillPanel(Variable[] result) {
        area.setText("");
        for (Variable v : result) {
            area.setText(area.getText() + v.toString() + "\n");
        }
    }

}
