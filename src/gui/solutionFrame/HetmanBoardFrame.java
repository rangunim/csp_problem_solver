package gui.solutionFrame;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;

import models.Variable;


public final class HetmanBoardFrame extends BoardFrame {
    public HetmanBoardFrame(List<Variable[]> results) {
        super(results);
        super.setTitle("N-Hetman");

        Variable[] result = iterator4Results.next();
        iterator4Results.previous();
        initProblemPanel(result.length * result.length);
        super.getContentPane().add(problemPanel, BorderLayout.CENTER);
        super.pack();
    }

    @Override
    protected void initProblemPanel(int boardSize) {
        problemPanel = new JPanel();
        int rowLength = (int) Math.sqrt(boardSize);
        problemPanel.setLayout(new GridLayout(rowLength, rowLength));
        problemPanel.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(3.0f)));

        f = new JTextField[boardSize];
        Font font1 = new Font("SansSerif", Font.PLAIN, 20);
        for (int i = 0; i < f.length; i++) {
            f[i] = new JTextField(2);
            f[i].setHorizontalAlignment(JTextField.CENTER);
            f[i].setFont(font1);
            f[i].setFocusable(false);
            problemPanel.add(f[i]);
        }
    }

    @Override
    protected void fillPanel(Variable[] result) {
        if (result[0].getValue() == null) return;

        Variable var;
        int nextRow = 0;
        for (int i = 0; i < result.length; i++) {
            var = result[i];
            int val = Integer.parseInt(String.valueOf(var.getValue()));

            for (int k = 0; k < val; k++) {
                f[k + nextRow].setText("");
            }
            f[val + nextRow - 1].setText("H");
            for (int k = val; k < result.length; k++) {
                f[k + nextRow].setText("");
            }
            nextRow += result.length;
        }
    }
}
