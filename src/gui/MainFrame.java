package gui;

import gui.solutionFrame.SudokuBoardFrame;
import gui.solutionFrame.HetmanBoardFrame;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import algorithms.Backtracking;
import algorithms.ForwardChecking;
import algorithms.Solver;
import helpers.FileHelper;
import models.Variable;
import gui.solutionFrame.BoardFrame;
import gui.solutionFrame.OtherBoardFrame;


public class MainFrame {
    private final JFrame mainFrame;

    private JPanel mainPanel;
    private JComboBox chooseAction;
    private JLabel numberOfSolutionsLabel;
    private JLabel timeLabel;

    private JPanel generateFilePanel;
    private JComboBox chooseProblem;
    private JComboBox sizeProblemChooser;

    private JPanel solutionPanel;
    private JTextField filePathField;
    private JComboBox chooseNumberOfSolutionInResult;
    private JComboBox chooseAlgorithm;
    private JButton startSolveButton;
    private JComboBox chooseHeureristic;
    private JButton showSolutionsButton;
    private JButton showGUISolutionsButton;

    private Solver solver;

    private final static String GENERATE_FILEPANEL_TITLE = "Generator pliku";
    private final static String CHOOSE_PROBLEM_TITLE = "Wybierz problem";
    private final static String[] TYPES_OF_PROBLEM = new String[]{"Hetman", "Sudoku"};
    private final static String SizeOfProblem_TITLE = "Wielkość problemu (N): ";


    //MAGIC NUMBER


    public MainFrame() {
        mainFrame = new JFrame("Problem spełniania ograniczeń (CSP)");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setMinimumSize(new Dimension(640, 330));
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setResizable(false);

        initGenerateFilePanel();
        initSolutionPanel();
        initMainPanel();

        JPanel internalPanel = new JPanel();
        internalPanel.setLayout(new GridBagLayout());
        addComponenet2GridBag(mainPanel, 0, 0, java.awt.GridBagConstraints.NORTHWEST, 4, 0, 1, internalPanel);

        mainFrame.getContentPane().add(new JScrollPane(internalPanel));
        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    private void initMainPanel() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());

        JTabbedPane internalPane = new JTabbedPane();
        internalPane.addTab("Wygeneruj plik CSP", generateFilePanel);
        internalPane.setMnemonicAt(0, KeyEvent.VK_1);

        internalPane.addTab("Rozwiąż problem", solutionPanel);
        internalPane.setMnemonicAt(0, KeyEvent.VK_2);
        addComponenet2GridBag(internalPane, 1, 1, java.awt.GridBagConstraints.EAST, 2, mainPanel);

        solutionPanel.setVisible(false);
    }


    private void initGenerateFilePanel() {
        generateFilePanel = new JPanel();
        generateFilePanel.setLayout(new GridBagLayout());
        generateFilePanel.setBorder(BorderFactory.createTitledBorder(GENERATE_FILEPANEL_TITLE));

        JLabel problemChooserLabel = new JLabel(CHOOSE_PROBLEM_TITLE);
        addComponenet2GridBag(problemChooserLabel, 0, 0, java.awt.GridBagConstraints.WEST, 1, generateFilePanel);

        chooseProblem = new JComboBox(TYPES_OF_PROBLEM);
        addComponenet2GridBag(chooseProblem, 1, 0, java.awt.GridBagConstraints.WEST, 1, generateFilePanel);

        JLabel sizeProblemLabel = new JLabel(SizeOfProblem_TITLE);
        addComponenet2GridBag(sizeProblemLabel, 0, 1, java.awt.GridBagConstraints.WEST, 1, generateFilePanel);

        sizeProblemChooser = new JComboBox();
        for (int i = 0; i < 50; i++) {
            sizeProblemChooser.addItem(i + 1);
        }
        addComponenet2GridBag(sizeProblemChooser, 1, 1, java.awt.GridBagConstraints.WEST, 1, generateFilePanel);

        JButton generateFileButton = new JButton("Generuj plik CSP");
        generateFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean b = FileHelper.generateCspFile(chooseProblem.getSelectedItem().toString(), Integer.parseInt(sizeProblemChooser.getSelectedItem().toString()));
                if (b)
                    JOptionPane.showMessageDialog(mainFrame, "Pomyślnie wygenerowano plik CSP o nazwie " + chooseProblem.getSelectedItem().toString());
                else
                    JOptionPane.showMessageDialog(mainFrame, "Nie udało się wygenerować pliku CSP o nazwie " + chooseProblem.getSelectedItem().toString());
            }
        });
        addComponenet2GridBag(generateFileButton, 0, 2, java.awt.GridBagConstraints.WEST, 2, generateFilePanel);


        JButton showBoardButton = new JButton("Pokaz planszę");
        showBoardButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Map<String, Variable> varFromFile;
                BoardFrame board = null;
                if (chooseProblem.getSelectedIndex() == 0) {
                    List<Variable[]> res = new ArrayList<>(2);
                    varFromFile = FileHelper.readCspFile("hetman.csp");
                    Variable[] r = new Variable[varFromFile.size()];
                    res.add(r);
                    board = new HetmanBoardFrame(res);
                } else if (chooseProblem.getSelectedIndex() == 1) {
                    varFromFile = FileHelper.readCspFile("sudoku.csp");
                    Variable[] r = new Variable[varFromFile.size()];
                    for (int i = 0; i < varFromFile.size(); i++) {
                        Variable v = varFromFile.get("V" + (i + 1));
                        if (v.getDomain().size() == 1) v.setNextValueFromDomain();
                        r[i] = v;
                    }
                    List<Variable[]> res = new ArrayList<>(2);
                    res.add(r);
                    board = new SudokuBoardFrame(res);
                } else {
                    List<Variable[]> res = new ArrayList<>(2);
                    board = new OtherBoardFrame(res);
                }
                board.setLocationRelativeTo(mainFrame);
            }
        });
        addComponenet2GridBag(showBoardButton, 1, 2, java.awt.GridBagConstraints.WEST, 2, generateFilePanel);
    }


    private void initSolutionPanel() {
        solutionPanel = new JPanel();
        solutionPanel.setLayout(new GridBagLayout());
        solutionPanel.setBorder(BorderFactory.createTitledBorder("Rozwiązywanie problemu"));

        JLabel fileLabel = new JLabel("Wczytaj plik CSP: ");
        addComponenet2GridBag(fileLabel, 0, 0, java.awt.GridBagConstraints.WEST, 1, solutionPanel);

        filePathField = new JTextField(20);
        filePathField.setEditable(false);
        addComponenet2GridBag(filePathField, 1, 0, java.awt.GridBagConstraints.WEST, 1, solutionPanel);

        JButton chooserButton = new JButton("Przeglądaj...");
        chooserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));
                chooser.addChoosableFileFilter(new FileNameExtensionFilter(".csp", "csp"));
                chooser.setAcceptAllFileFilterUsed(false);

                int returnVal = chooser.showOpenDialog(solutionPanel);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    try {
                        filePathField.setText(chooser.getSelectedFile().getCanonicalPath());
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(mainFrame, "Błąd podczas wyboru pliku CSP. Szczegóły: \n" + ex.getMessage());
                    }
                }
            }
        });
        addComponenet2GridBag(chooserButton, 2, 0, java.awt.GridBagConstraints.WEST, 1, solutionPanel);

        JLabel countSolutionInResultLabel = new JLabel("Liczba rozwiązań? ");
        addComponenet2GridBag(countSolutionInResultLabel, 0, 1, java.awt.GridBagConstraints.WEST, 1, solutionPanel);

        chooseNumberOfSolutionInResult = new JComboBox(new String[]{"1", "Wszystkie"});
        addComponenet2GridBag(chooseNumberOfSolutionInResult, 1, 1, java.awt.GridBagConstraints.WEST, 1, solutionPanel);

        JLabel chooseAlgorithmLabel = new JLabel("Wybierz algorytm: ");
        addComponenet2GridBag(chooseAlgorithmLabel, 0, 2, java.awt.GridBagConstraints.WEST, 1, solutionPanel);

        chooseAlgorithm = new JComboBox(new String[]{"Backtracking", "Forward Checking"});
        addComponenet2GridBag(chooseAlgorithm, 1, 2, java.awt.GridBagConstraints.WEST, 1, solutionPanel);

        JLabel heureristicsLabel = new JLabel("Heurystyka: ");
        addComponenet2GridBag(heureristicsLabel, 0, 3, java.awt.GridBagConstraints.WEST, 1, solutionPanel);

        chooseHeureristic = new JComboBox(new String[]{"Zmienna z najw. ilością ograniczeń", "Zmienne z najmniejszą dziedziną", "Brak"});
        addComponenet2GridBag(chooseHeureristic, 1, 3, java.awt.GridBagConstraints.WEST, 1, solutionPanel);

        startSolveButton = new JButton("Rozpocznij działanie");
        startSolveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startSolveProblem(e);
            }
        });
        addComponenet2GridBag(startSolveButton, 0, 4, java.awt.GridBagConstraints.WEST, 1, solutionPanel);

        showSolutionsButton = new JButton("Pokaż rozwiązania w kosnoli");
        showSolutionsButton.setVisible(false);
        showSolutionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                solver.showResults();
            }
        });
        addComponenet2GridBag(showSolutionsButton, 1, 4, java.awt.GridBagConstraints.WEST, 1, 4, 0, solutionPanel);

        showGUISolutionsButton = new JButton("Pokaż rozwiązania W GUI");
        showGUISolutionsButton.setVisible(false);
        showGUISolutionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Variable[]> res = solver.getResults();
                BoardFrame board;
                String fileName = filePathField.getText();
                if (fileName.toLowerCase().endsWith("hetman.csp")) {
                    board = new HetmanBoardFrame(res);
                } else if (fileName.toLowerCase().endsWith("sudoku.csp")) {
                    board = new SudokuBoardFrame(res);
                } else {
                    board = new OtherBoardFrame(res);
                }
                board.setLocationRelativeTo(mainFrame);
            }
        });
        addComponenet2GridBag(showGUISolutionsButton, 2, 4, java.awt.GridBagConstraints.WEST, 1, 4, 0, solutionPanel);

        numberOfSolutionsLabel = new JLabel();
        addComponenet2GridBag(numberOfSolutionsLabel, 0, 5, java.awt.GridBagConstraints.WEST, 3, solutionPanel);

        timeLabel = new JLabel();
        addComponenet2GridBag(timeLabel, 1, 5, java.awt.GridBagConstraints.EAST, 3, solutionPanel);
    }

    private void addComponenet2GridBag(Component component, int gridX, int gridY, int constraint, int gridwidth, JPanel panel) {
        addComponenet2GridBag(component, gridX, gridY, constraint, gridwidth, 0, 0, panel);
    }

    private void addComponenet2GridBag(Component component, int gridX, int gridY, int constraint, int gridwidth, int weigthx, int weigthy, JPanel panel) {
        java.awt.GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = gridX;
        gridBagConstraints.gridy = gridY;
        gridBagConstraints.anchor = constraint;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        gridBagConstraints.gridwidth = gridwidth;
        gridBagConstraints.weightx = weigthx;
        gridBagConstraints.weighty = weigthy;
        panel.add(component, gridBagConstraints);
    }

    private void startSolveProblem(ActionEvent e) {
        showSolutionsButton.setVisible(false);
        showGUISolutionsButton.setVisible(false);
        Map<String, Variable> varFromFile = FileHelper.readCspFile(filePathField.getText());
        solver = chooseAlgorithm.getSelectedIndex() == 0 ? new Backtracking(varFromFile, chooseHeureristic.getSelectedIndex()) : new ForwardChecking(varFromFile, chooseHeureristic.getSelectedIndex());
        long start = System.currentTimeMillis();
        if (chooseNumberOfSolutionInResult.getSelectedIndex() == 0) {
            solver.solveOne();
        } else {
            solver.solveAll();
        }
        numberOfSolutionsLabel.setText("Liczba rozwiązań: " + solver.getResults().size());
        timeLabel.setText("Czas: " + (System.currentTimeMillis() - start) + "[ms]");
        showSolutionsButton.setVisible(true);
        showGUISolutionsButton.setVisible(true);
    }


}
