package algorithms;

import java.util.Map;

import models.Variable;

public class Backtracking extends Solver {
    public Backtracking(Map<String, Variable> variablesFromFile, int choosedHeureristic) {
        super(variablesFromFile, choosedHeureristic);
    }

    @Override
    public void solveOne() {
        search(1, true);
    }

    @Override
    public void solveAll() {
        search(1, false);
    }

    private void search(int c, boolean onlyOneSolution) {
        Variable var;
        if (c > sortedVariables.size()) {
            if (!onlyOneSolution) {
                sortedVariables.get(c - 2).setValue(null);
            }
        } else {
            var = sortedVariables.get(c - 1);
            while (var.isNextValueInDomain()) {
                if (onlyOneSolution && !results.isEmpty()) {
                    return;
                }

                var.setNextValueFromDomain();
                if (checkConstraints(var.getConstraints())) {
                    if (c == sortedVariables.size()) {
                        addSolution2Results();
                    }
                    search(c + 1, onlyOneSolution);
                } else {
                    var.setValue(null);
                }
            }
            if (var.getValue() == null) {
                if (c > 1) {
                    var.restVariable();
                    sortedVariables.get(c - 2).setValue(null);
                }
            }
        }
    }
}
