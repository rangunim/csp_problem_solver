package algorithms.heureristics;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.Variable;


public class Heureristic {
    public static List<Variable> getChoosedHeureristic(Map<String, Variable> variablesFromFile, int choosedHeureristic) {
        List<Variable> result = new LinkedList<>();
        for (int i = 0; i < variablesFromFile.size(); i++) {
            result.add(variablesFromFile.get("V" + (i + 1)));
        }

        return getChoosedHeureristic(result, choosedHeureristic);
    }

    public static List<Variable> getChoosedHeureristic(List<Variable> variables, int choosedHeureristic) {
        switch (choosedHeureristic) {
            case 0:
                sortVariablesByMaxConstraints(variables);
                break;
            case 1:
                sortVariablesByMinDomains(variables);
                break;
            default:
                break;
        }
        return variables;
    }

    private static void sortVariablesByMaxConstraints(List<Variable> var) {
        Comparator c = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Variable v1 = (Variable) o1;
                Variable v2 = (Variable) o2;
                Set<String> v1Const = v1.getConstraints();
                Set<String> v2Const = v2.getConstraints();

                if (v1Const.size() < v2Const.size()) {
                    return -1;
                } else if (v1Const.size() == v2Const.size()) {
                    return v1.getName().compareTo(v2.getName());
                } else
                    return 1;
            }
        };
        Collections.sort(var, c);
    }

    private static void sortVariablesByMinDomains(List<Variable> var) {
        Comparator c = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return calculateCompareValue((Variable) o1, (Variable) o2, ((Variable) o1).getDomain(), ((Variable) o2).getDomain());
            }
        };
        Collections.sort(var, c);
    }

    private static int calculateCompareValue(Variable v1, Variable v2, Set<Object> d1, Set<Object> d2) {
        if (d1.size() < d2.size()) {
            return -1;
        } else if (d1.size() == d2.size()) {
            return v1.getName().compareTo(v2.getName());
        } else {
            return 1;
        }
    }

    public static void sortVariablesByMaxUsedValues(List<Variable> var) {
        Comparator c = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return calculateCompareValue((Variable) o1, (Variable) o2, ((Variable) o1).getUsedValues(), ((Variable) o2).getUsedValues()) * -1;
            }
        };
        Collections.sort(var, c);
    }

}
