package algorithms;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import algorithms.heureristics.Heureristic;
import models.Variable;


public class ForwardChecking extends Solver {

    public ForwardChecking(Map<String, Variable> variablesFromFile, int choosedHeureristic) {
        super(variablesFromFile, choosedHeureristic);
    }

    @Override
    public void solveOne() {
        search(1, true);
    }

    @Override
    public void solveAll() {
        search(1, false);
    }


    private void search(int c, boolean onlyOneSolution) {
        Variable var;
        if (c > sortedVariables.size()) {
            if (!onlyOneSolution) {
                removeLastConstraint(c - 2);
            }
        } else {
            var = sortedVariables.get(c - 1);
            while (var.isNextValueInDomain()) {
                if (onlyOneSolution && !results.isEmpty()) {
                    return;
                }

                var.setNextValueFromDomain();
                if (c == sortedVariables.size()) {
                    addSolution2Results();
                } else {
                    setConstraints4Forward();
                }
                search(c + 1, onlyOneSolution);
            }

            if (var.getValue() == null) {
                if (c > 1) {
                    removeLastConstraint(c - 2);
                }
            }
        }
    }


    private void clearConstraintsFromUnassignedVariables() {
        for (Variable v : sortedVariables) {
            if (v.getValue() == null) {
                v.restVariable();
            }
        }
    }


    private void setConstraints4Forward() {
        List<Variable> unassignedVars = new LinkedList<>();
        List<Variable> result = new LinkedList<>();
        for (Variable v : sortedVariables) {
            if (v.getValue() == null) {
                unassignedVars.add(v);
            } else {
                result.add(v);
            }
        }

        for (Variable v : unassignedVars) {
            Set<Object> usedValues = v.copy().getUsedValues();
            Set<Object> unacceptedValues = new HashSet<>();
            while (v.isNextValueInDomain()) {
                v.setNextValueFromDomain();
                if (!checkConstraints(v.getConstraints())) {
                    unacceptedValues.add(v.getValue());
                }
                v.setValue(null);
            }
            v.setUsedValues(unacceptedValues);

            unacceptedValues = v.getUsedValues();
            for (Object usedValue : usedValues) {
                unacceptedValues.add(usedValue);
            }
        }

        if (choosedHeureristic == 1) {
            Heureristic.sortVariablesByMaxUsedValues(unassignedVars);
            for (Variable v : unassignedVars) {
                result.add(v);
            }
            sortedVariables = result;
        }
    }

    private void removeLastConstraint(int c) {
        Variable var = sortedVariables.get(c).copy();
        Object actualValue = var.getValue();

        var = sortedVariables.get(c);
        clearConstraintsFromUnassignedVariables();
        var.setValue(null);
        setConstraints4Forward();
        var.getUsedValues().add(actualValue);
    }


}
