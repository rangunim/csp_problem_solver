package algorithms;

import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import javax.swing.JOptionPane;

import algorithms.heureristics.Heureristic;
import models.Variable;


public abstract class Solver {
    protected Map<String, Variable> variablesFromFile;
    protected List<Variable> sortedVariables;
    protected final List<Variable[]> results;
    protected int choosedHeureristic;

    public Solver(Map<String, Variable> variablesFromFile, int choosedHeureristic) {
        this.variablesFromFile = variablesFromFile;
        this.choosedHeureristic = choosedHeureristic;
        this.sortedVariables = Heureristic.getChoosedHeureristic(variablesFromFile, choosedHeureristic);
        results = new LinkedList<>();
    }

    protected boolean checkConstraints(Set<String> constraints) {
        for (String str : constraints) {
            if (!analyzeONPExpression(str)) {
                return false;
            }
        }
        return true;
    }


    private boolean analyzeONPExpression(String expression) {
        Stack<Object> stack = new Stack<>();
        String[] cons = expression.split(" ");

        for (String con : cons) {
            if (con.charAt(0) == 'V') //jesli to zmienna
            {
                Object value = variablesFromFile.get(con).getValue();
                if (value == null) // jesli zmienna jeszcze nie ma przypisanej wartosci to pomin to ograniczenie (chwilowo sie zgadza)
                {
                    return true;
                } else {
                    stack.push(String.valueOf(value));
                }
            } else if (Character.isDigit(con.charAt(0))) // jest sta�� z pliku np. 1.
            {
                stack.push(con);
            } else // jesli operator to oblicz i/lub sprawdz ograniczenie
            {
                //****** POWTARZAJACE SIE FRAGMENTY  W WYR WARUNKOWYM !!!!!!!! ***
                try {
                    String result;
                    double arg1 = getDoubleFromString((String) stack.pop());
                    if (con.equals("||")) {
                        result = Math.abs(arg1) + "";
                    } else {
                        double arg2 = getDoubleFromString((String) stack.pop());

                        switch (con) {
                            case "+":
                                result = arg1 + arg2 + "";
                                break;
                            case "-":
                                result = arg1 - arg2 + "";
                                break;
                            case "*":
                                result = arg1 * arg2 + "";
                                break;
                            case ">":
                                result = (arg1 > arg2) + "";
                                break;
                            case "<":
                                result = (arg1 < arg2) + "";
                                break;
                            case "=":
                                result = (arg1 == arg2) + "";
                                break;
                            case "<>":
                                result = (arg1 != arg2) + "";
                                break;
                            default:
                                JOptionPane.showMessageDialog(null, "Wyrazenie nie jest niepoprawne");
                                result = null;
                        }
                    }
                    //**************************************************************************

                    if (con.equals("+") || con.equals("-") || con.equals("*") || con.equals("||")) {
                        stack.push(result);
                    } else {
                        if (!Boolean.valueOf(result)) {
                            return false;
                        }
                    }
                } catch (EmptyStackException e) {
                    JOptionPane.showMessageDialog(null, "Bledne wyrazenie");
                    return false;
                }
            }
        }
        return true;
    }

    private double getDoubleFromString(String s) {
        return Double.parseDouble(s);
    }

    public void showResults() {
        Iterator<Variable[]> it = results.iterator();
        int count = 0;
        while (it.hasNext()) {
            Variable[] temp = it.next();
            System.out.print("Rozwiazanie " + ++count + " : [");
            for (int i = 0; i < temp.length; i++) {
                System.out.print(temp[i] + " ");
            }
            System.out.println("]");
        }
    }

    protected void addSolution2Results() {
        Variable[] result = new Variable[variablesFromFile.size()];
        for (int i = 0; i < variablesFromFile.size(); i++) // czy same values do results?
        {
            result[i] = variablesFromFile.get("V" + (i + 1)).copy();
        }
        results.add(result);
    }

    public abstract void solveAll();

    public abstract void solveOne();


    public List<Variable[]> getResults() {
        return results;
    }

}



/*


 private boolean analyzeONPExpression(String expression)
    {
        Stack<Object> stack = new Stack<>();
        String [] cons = expression.split(" ");
        
        for (String con : cons) 
        {
            if(con.charAt(0)== 'V') //jesli to zmienna
            {
                Object value = variablesFromFile.get(con).getValue();
                if(value == null) // jesli zmienna jeszcze nie ma przypisanej wartosci to pomin to ograniczenie (chwilowo sie zgadza)
                {
                    return true;
                }
                else
                {
                    stack.push(String.valueOf(value));
                }
            }
            else if(Character.isDigit(con.charAt(0))) // jest sta�� z pliku np. 1.
            {
                stack.push(con);
            }
            else // jesli operator to oblicz i/lub sprawdz ograniczenie
            {
                try
                {
                    String result;
                    if(con.equals("||"))
                    {
                        result = doOperation(con, new Object [] { stack.pop() } );  
                    }
                    else
                    {
                        result = doOperation(con, new Object[] { stack.pop(), stack.pop() });
                    }

                    if(con.equals("+") || con.equals("-") || con.equals("*") || con.equals("||"))
                    {
                        stack.push(result);
                    }
                    else
                    {
                        if(!Boolean.valueOf(result))
                        {
                            return false;
                        }
                    } 
                }
                catch(EmptyStackException e)
                {
                    JOptionPane.showMessageDialog(null,"Bledne wyrazenie");
                    return false;
                }
            }
        }
        return true;
    }
    
    private static String doOperation(String operation, Object[] args)
    {
        switch(operation) 
        {
            case "+":
                    return String.valueOf(Double.parseDouble((String)args[1]) + Double.parseDouble((String)args[0]));
            case "-":
                    return String.valueOf(Double.parseDouble((String)args[1]) - Double.parseDouble((String)args[0]));
            case "*":
                    return String.valueOf(Double.parseDouble((String)args[1]) * Double.parseDouble((String)args[0]));
            case ">":
                    return String.valueOf(Double.parseDouble((String)args[1]) > Double.parseDouble((String)args[0]));
            case "<":
                    return String.valueOf(Double.parseDouble((String)args[1]) < Double.parseDouble((String)args[0]));
            case "=":
                    return String.valueOf(Double.parseDouble((String)args[1]) == Double.parseDouble((String)args[0]));
            case "<>":                    
                    return String.valueOf(Double.parseDouble((String)args[1]) != Double.parseDouble((String)args[0]));
            case "||":
                    return String.valueOf(Math.abs(Double.parseDouble((String)args[0])));
            default:
                  JOptionPane.showMessageDialog(null, "Wyrazenie nie jest niepoprawne");
                  return null;
        }
    }
*/



/*  KOPIA 2
              try
                {
                    String result;
                    if(con.equals("||"))
                    {
                        result = String.valueOf(Math.abs(Double.parseDouble((String) stack.pop())));
                    }
                    else
                    {
                        switch(con) 
                        {
                            case "+":
                                    result = String.valueOf(Double.parseDouble((String) stack.pop()) + Double.parseDouble((String) stack.pop())); break;
                            case "-":
                                    result = String.valueOf(Double.parseDouble((String) stack.pop()) - Double.parseDouble((String) stack.pop())); break;
                            case "*":
                                    result = String.valueOf(Double.parseDouble((String) stack.pop()) * Double.parseDouble((String) stack.pop())); break;
                            case ">":
                                    result = String.valueOf(Double.parseDouble((String) stack.pop()) > Double.parseDouble((String) stack.pop())); break;
                            case "<":
                                    result = String.valueOf(Double.parseDouble((String) stack.pop()) < Double.parseDouble((String) stack.pop())); break;
                            case "=":
                                    result = String.valueOf(Double.parseDouble((String) stack.pop()) == Double.parseDouble((String) stack.pop())); break;
                            case "<>":                    
                                    result = String.valueOf(Double.parseDouble((String) stack.pop()) != Double.parseDouble((String) stack.pop())); break;
                            default:
                                  JOptionPane.showMessageDialog(null, "Wyrazenie nie jest niepoprawne");
                                  result = null;
                        }
                    }

                    if(con.equals("+") || con.equals("-") || con.equals("*") || con.equals("||"))
                    {
                        stack.push(result);
                    }
                    else
                    {
                        if(!Boolean.valueOf(result))
                        {
                            return false;
                        }
                    } 
                }
                catch(EmptyStackException e)
                {
                    JOptionPane.showMessageDialog(null,"Bledne wyrazenie");
                    return false;
                }
*/