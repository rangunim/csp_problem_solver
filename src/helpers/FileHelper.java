package helpers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import javax.swing.JOptionPane;

import models.Variable;


public class FileHelper {
    public static boolean generateCspFile(String problem, int size) {
        try {
            switch (problem) {
                case "Hetman":
                    return generateCspFile4HetmanProblem(size);
                case "Sudoku":
                    return generateCspFile4Sudoku(size);
                default:
                    JOptionPane.showMessageDialog(null, "Error w generateCSPFile: Nieznany typ problemu!");
                    return false;
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Error w generateCSPFile: I/O exception. Szczegóły: " + e.getMessage());
            return false;
        }
    }

    private static boolean generateCspFile4HetmanProblem(int size) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("hetman.csp")))) {
            for (int i = 0; i < size; i++)            //zmienne
            {
                writer.write("V" + (i + 1) + " ");
            }
            writer.newLine();

            for (int i = 0; i < size; i++)            //dziedziny
            {
                for (int j = 0; j < size; j++) {
                    writer.write((j + 1) + " ");
                }
                writer.newLine();
            }

            for (int i = 0; i < size; i++)            //ograniczenie - rząd
            {
                for (int j = i + 1; j < size; j++) {
                    writer.write("V" + (i + 1) + " ");
                    writer.write("V" + (j + 1) + " ");
                    writer.write("<>");
                    writer.newLine();
                }
            }

            for (int i = 0; i < size; i++)            //ograniczenie - przekątna
            {
                for (int j = i + 1; j < size; j++) {
                    writer.write("V" + (i + 1) + " ");
                    writer.write("V" + (j + 1) + " ");
                    writer.write("- || " + (j - i) + " <>");
                    writer.newLine();
                }
            }
        }
        return true;
    }

    private static boolean generateCspFile4Sudoku(int size) throws IOException {
        String path = System.getProperty("user.dir") + File.separator + "sudoku_boards" + File.separator + size;
        int numberOfStaticVariables = size * (int) Math.sqrt(size);
        Random rand = new Random();
        int boards;
        String board = "";
        try
        {
            boards = new File(path).list().length;
            BufferedReader br = new BufferedReader(new FileReader(path + File.separator + "sudoku_" + (rand.nextInt(boards) + 1) + ".txt"));
                String line;
            while ((line = br.readLine()) != null) {
                    board += line + "\n";
                }
            br.close();

        }
        catch(IOException | NullPointerException e)
        {
            String msg = "Błąd I/O podczas odczytu pliku " + path + ". Szczegóły: " + e.getMessage() != "null" && e.getMessage() != null ? e.getMessage() : "Nie znaleziono takiego pliku/ścieżki! ";
            JOptionPane.showMessageDialog(null, msg);
            throw new InternalError("FileHelper.generateCspFile4Sudoku:" + msg);
        }


        int counter = 0;
        //int temp = 0;
        String[] spliter = board.replace("\n", " ").split(" ");
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("sudoku.csp")))) {
            for (int i = 0; i < size * size; i++)            //zmienne
            {
                writer.write("V" + (i + 1) + " ");
            }
            writer.newLine();

            for (int i = 0; i < size * size; i++)            //dziedziny
            {
                if (counter < numberOfStaticVariables && rand.nextInt(size) + 1 <= Math.sqrt(size))   //stale - pola wypelnione
                {
                    writer.write(spliter[i]);
                    counter++;
                } else {
                    for (int j = 0; j < size; j++) {
                        writer.write((j + 1) + " ");
                    }
                }
                writer.newLine();
            }


            for (int i = 0; i < size; i++) // nr rzędu			//ograniczenie - rząd
            {
                for (int j = i * size + 1; j < (i + 1) * size + 1; j++) // zmienne wybranego rzedu
                {
                    for (int k = j + 1; k < (i + 1) * size + 1; k++) {
                        writer.write("V" + j + " ");
                        writer.write("V" + k + " ");
                        writer.write("<>");
                        writer.newLine();
                    }
                }
            }

            for (int col = 0; col < size; col++) //ograniczenie - kolumna
            {
                for (int i = 0; i < size * size; i += size) {
                    for (int j = i * size + 1; j < size * size; j += size) {
                        for (int k = j + size; k < size * size; k += size) {
                            writer.write("V" + (j + col) + " ");
                            writer.write("V" + (k + col) + " ");
                            writer.write("<>");
                            writer.newLine();
                        }
                    }
                }
            }

            int indexNextSubSquare = 0;
            List<int[]> subSquares = new ArrayList<>(size);
            for (int i = 1; i <= size; i++)     //ograniczenie - podkwadrat - etap I generowanie listy.
            {
                int[] subSquare = new int[size];
                int index = 0;
                for (int j = 0; j < (int) Math.sqrt(size); j++) {
                    for (int k = 0; k < (int) Math.sqrt(size); k++) // daje kolejny podkwadrat
                    {
                        //System.out.print((j * size + k + 1 + indexNextSubSquare) + " ");
                        subSquare[index++] = j * size + k + 1 + indexNextSubSquare;
                    }
                    //   System.out.println();
                }
                //System.out.println("Koniec podkwadratu " + i);
                subSquares.add(subSquare);
                if (i % (int) Math.sqrt(size) == 0) {
                    indexNextSubSquare = i * size;
                } else {
                    indexNextSubSquare += (int) Math.sqrt(size);
                }
            }

            for (int[] subSquare : subSquares) //  etap II - zapis ograniczen podkwadratow do pliku
            {
                for (int i = 0; i < subSquare.length; i++) {
                    for (int j = i + 1; j < subSquare.length; j++) {
                        if (Math.abs(subSquare[j] - subSquare[i]) > (int) Math.sqrt(size) && Math.abs(subSquare[j] - subSquare[i]) % size != 0) // bo sa powtorki zwiazne z rz�dem/kolumn�
                        {
                            //System.out.println(subSquare[i] + " " + subSquare[j] + " <> ");
                            writer.write("V" + subSquare[i] + " ");
                            writer.write("V" + subSquare[j] + " ");
                            writer.write("<>");
                            writer.newLine();
                        }
                    }
                }
            }
        }
        return true;
    }


    public static Map<String, Variable> readCspFile(String fileName) {
        Map<String, Variable> variables = null;
        String line;
        int numOfVariables = 0;

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            if ((line = br.readLine()) != null) {
                numOfVariables = line.split(" ").length;
                variables = new HashMap<>(numOfVariables);
                for (int i = 0; i < numOfVariables; i++) {
                    variables.put("V" + (i + 1), new Variable("V" + (i + 1)));
                }
            }

            Set<Object> domain;
            for (int i = 0; i < numOfVariables; i++) {
                if ((line = br.readLine()) != null) {
                    domain = new HashSet<>();
                    for (String dom : line.split(" ")) {
                        domain.add(Integer.valueOf(dom));
                    }
                    variables.get("V" + (i + 1)).setDomain(domain);
                }
            }


            while ((line = br.readLine()) != null) {
                String[] spliter = line.split("V");
                boolean endLocal;
                String var;
                for (int c = 0; c < spliter.length; c++) {
                    char[] v = spliter[c].toCharArray();
                    var = "V";
                    endLocal = false;
                    for (int x = 0; !endLocal && x < v.length; x++) {
                        if (Character.isDigit(v[x])) {
                            var += v[x];
                        } else {
                            endLocal = true;
                        }
                    }

                    Variable temp = variables.get(var);
                    if (temp != null) {
                        temp.addConstraints(line);
                    }
                }
            }
            br.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Błąd I/O podczas odczytu pliku " + fileName + ". Szczegóły: " + e.getMessage());
        }

        return variables;
    }
}
