package models;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;


public class Variable {
    private String name;
    private Object value;
    private Set<Object> domain;
    private Set<String> constraints;
    private Set<Object> usedValues;


    public Variable(String name) {
        this.name = name;
        domain = new HashSet<>();
        constraints = new HashSet<>();
        usedValues = new HashSet<>();
    }

    public Variable(Variable v) {
        this.name = v.getName();
        this.value = v.getValue();

        Set<Object> d = new HashSet<>();

        Iterator it = v.getDomain().iterator();
        while (it.hasNext()) {
            d.add(it.next());
        }
        this.domain = d;

        Set<String> c = new HashSet<>();
        it = v.getConstraints().iterator();
        while (it.hasNext()) {
            c.add((String) it.next());
        }
        this.constraints = c;

        Set<Object> u = new HashSet<>();
        it = v.getUsedValues().iterator();
        while (it.hasNext()) {
            u.add(it.next());
        }
        this.usedValues = u;
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }

    public Set<Object> getDomain() {
        return domain;
    }

    public Set<String> getConstraints() {
        return constraints;
    }

    public Set<Object> getUsedValues() {
        return Collections.unmodifiableSet(usedValues);
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public void setDomain(Set<Object> domain) {
        this.domain = domain;
    }

    public void setConstraints(Set<String> constraints) {
        this.constraints = constraints;
    }

    public void setUsedValues(Set<Object> usedValues) {
        this.usedValues = usedValues;
    }

    public Variable copy() {
        return new Variable(this);
    }


    public void addConstraints(String constraint) {
        this.constraints.add(constraint);
    }

    public boolean isNextValueInDomain() {
        return domain.size() > usedValues.size();
    }

    public void setNextValueFromDomain() {
        if (isNextValueInDomain()) {
            Iterator<Object> it = domain.iterator();
            boolean endLocalLoop = false;
            while (it.hasNext() && !endLocalLoop) {
                Object temp = it.next();
                if (!usedValues.contains(temp)) {
                    value = temp;
                    usedValues.add(temp);
                    endLocalLoop = true;
                }
            }
        }
    }

    public Object removeValue() {
        Object result = value;
        usedValues.remove(result);
        value = null;
        return result;
    }

    public void restVariable() {
        value = null;
        usedValues.clear();
    }


    @Override
    public String toString() {
        return name + " := " + value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        return name.equals(((Variable) obj).name);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.name);
        return hash;
    }


}
